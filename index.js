const textFirstName = document.querySelector('#txt-first-name');
const textLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');


textFirstName.addEventListener("input", addFirstAndLastName);
textLastName.addEventListener("input", addFirstAndLastName);



function addFirstAndLastName() {
	const firstName = textFirstName.value;	
	const lastName = textLastName.value;	
	
	const fullName = `${firstName} ${lastName}`

	spanFullName.innerHTML = fullName
}
